import io
import cv2
import torch
from torch import nn, Tensor
from torch.nn import functional as F
import numpy as np
import albumentations as A
from typing import Tuple

from PIL import Image
from torchvision import transforms

import json
import io

from starlette.responses import Response

from fastapi import FastAPI, File

# adapted from https://pytorch.org/hub/pytorch_vision_deeplabv3_resnet101/

import torch
import torch.nn as nn


def double_conv(in_channels, out_channels):
    return nn.Sequential(
        nn.Conv2d(in_channels, out_channels, 3, padding=1),
        nn.BatchNorm2d(out_channels),
        nn.ReLU(inplace=True),
        nn.Conv2d(out_channels, out_channels, 3, padding=1),
        nn.BatchNorm2d(out_channels),
        nn.ReLU(inplace=True)
    )


class UNet(nn.Module):

    def __init__(self, n_class):
        super().__init__()

        self.dconv_down1 = double_conv(3, 64)
        self.dconv_down2 = double_conv(64, 128)
        self.dconv_down3 = double_conv(128, 256)
        self.dconv_down4 = double_conv(256, 512)
        self.dconv_down5 = double_conv(512, 1024)

        self.maxpool = nn.MaxPool2d(2)
        self.upsample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)

        self.dconv_up4 = double_conv(512 + 1024, 512)
        self.dconv_up3 = double_conv(256 + 512, 256)
        self.dconv_up2 = double_conv(128 + 256, 128)
        self.dconv_up1 = double_conv(128 + 64, 64)

        self.conv_last = nn.Conv2d(64, n_class, 1)

    def forward(self, x):
        conv1 = self.dconv_down1(x)
        x = self.maxpool(conv1)

        conv2 = self.dconv_down2(x)
        x = self.maxpool(conv2)

        conv3 = self.dconv_down3(x)
        x = self.maxpool(conv3)

        conv4 = self.dconv_down4(x)
        x = self.maxpool(conv4)

        x = self.dconv_down5(x)

        x = self.upsample(x)
        x = torch.cat([x, conv4], dim=1)
        x = self.dconv_up4(x)

        x = self.upsample(x)
        x = torch.cat([x, conv3], dim=1)

        x = self.dconv_up3(x)
        x = self.upsample(x)
        x = torch.cat([x, conv2], dim=1)

        x = self.dconv_up2(x)
        x = self.upsample(x)
        x = torch.cat([x, conv1], dim=1)

        x = self.dconv_up1(x)

        x = self.conv_last(x)

        return x


def onehot_to_img(mask):
  #Only for displaying and testing
  transition_matrix = torch.Tensor(np.asarray([[0,0,0],
                              [0,0,255],
                              [0,255,0],
                              [0,255,255],
                              [255,0,0],
                              [255,0,255],
                              [255,255,0],
                              [255,255,255]])).int()
  return torch.matmul(mask,transition_matrix)



model = UNet(8)
model.load_state_dict(torch.load("./SUIM_Unet.pth", map_location=torch.device("cpu")))
model.eval()
app = FastAPI()



def get_segments(model, binary_image, max_size=512):
    sigmoid = nn.Sigmoid()

    input_image = Image.open(io.BytesIO(binary_image)).convert("RGB")
    image = np.array(input_image)

    image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_CUBIC).astype("float32")/255
    transform = transforms.Normalize([0.5, 0.5, 0.5], [0.5, 0.5, 0.5])
    image_tensor = transform(torch.from_numpy(np.transpose(image, (2, 0, 1))))







    with torch.no_grad():
        output = model(image_tensor.unsqueeze(0))

    out = output.squeeze(0).permute(1,2,0).argmax(2) #Return to index image
    # out = F.one_hot(out.long(), 8).int()  #To one-hot representation
    # out_mask = onehot_to_img(out).numpy().astype("uint8")



    # return Image.fromarray(predicted_img)
    return out


@app.post("/segmentation")
def get_segmentation_map(file: bytes = File(...)):
    """Get segmentation maps from image file"""
    out = F.one_hot(get_segments(model, file).long(), 8).int()  #To one-hot representation
    out_mask = onehot_to_img(out).numpy().astype("uint8")
    segmented_image = Image.fromarray(out_mask)
    bytes_io = io.BytesIO()
    segmented_image.save(bytes_io, format="PNG")
    return Response(bytes_io.getvalue(), media_type="image/png")


@app.post("/index")
def get_index_map(file: bytes = File(...)):
    """Get segmentation maps from image file"""
    segmented_image = get_segments(model, file)
    return Response(json.dumps(segmented_image.tolist()))